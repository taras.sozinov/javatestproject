Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for existing product
    When he calls endpoint for '<product name>'
    Then he sees the results displayed for '<product name>'

    Examples:
    | product name |
    | orange       |
    | apple        |
    | pasta        |
    | cola         |

  Scenario: Search for not existing product
    When he calls endpoint for 'car'
    Then he does not see the results

  Scenario: Post method is not allowed
    When he calls endpoint 'orange' with post
    Then he see that method is not allowed


