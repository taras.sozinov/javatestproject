package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static com.google.common.base.Predicates.equalTo;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

public class SearchStepDefinitions {

    final String baseUrl = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

    @Steps
    public CarsAPI carsAPI;

    @When("he calls endpoint for {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(baseUrl + arg0);
    }

    @When("he calls endpoint {string} with post")
    public void heCallsEndpointWthPost(String arg0) {
        SerenityRest.given().post(baseUrl + arg0);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForProductName(String arg0) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("title", contains(arg0)));
    }

    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> {
            response.body("detail.error",  is(true));
        });
    }

    @Then("he see that method is not allowed")
    public void heIsNotAllowedToDoThat() {
        restAssuredThat(response -> response.statusCode(405));
        restAssuredThat(response -> {
            response.body("detail",  is("Method Not Allowed"));
        });
    }

    @Then("he sees the results displayed for <product name>")
    public void he_Sees_TheResultsDisplayedForProductName() {
    }
}
