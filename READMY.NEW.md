Test task.

1. What were updated:
   1.1 Remove gradle from project
   1.2 Add Cucumber data tables
   1.3 Create negative scenarios
   1.4 Remove unused code
   1.5 Setup CI with reports